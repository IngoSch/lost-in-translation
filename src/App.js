import './App.css';
import {
  BrowserRouter,
  Switch,
  Route
} from 'react-router-dom'
import Login from './components/Login/Login'
import NotFound from './components/NotFound/NotFound'
import Translator from './components/Translator/Translator';
import Profile from './components/Profile/Profile';

function App() {
  return (
    
    <BrowserRouter>
    
      <div className="App">
        
        <Switch>
          <Route path="/profile" component={Profile} />
          <Route path="/translator" component={Translator} />
          <Route path="/" exact component={ Login } />
          <Route path="*" component={ NotFound } />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
