const jsonServer = require('json-server')
const server = jsonServer.create()
const path = require('./src/server/')
const router = jsonServer.router(path.join(__dirname, 'db.json'))
const middlewares = jsonServer.defaults()
 
server.use(middlewares)
server.use(router)
server.listen(3001, () => {
  console.log('JSON Server is running')
})

//exportieren und dann starten heraussuchen von Code.
