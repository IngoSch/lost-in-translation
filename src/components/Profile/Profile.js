import { useDispatch, useSelector } from "react-redux"
import AppContainer from "../../hoc/AppContainer"
import { Redirect } from "react-router"
import { profileClearAction, profileLogoutAction } from "../../store/actions/profileActions"


const Profile = () => {
    const dispatch = useDispatch()
    const { loggedIn } = useSelector(state => state.translatorReducer)
    const array =JSON.parse(localStorage.getItem('tr-ph'))

  

    const clearPhrases = event => {
        console.log(array)
        dispatch(profileClearAction())
    }

    const doLogout = event => {
        dispatch(profileLogoutAction())


    }


    return (
        <>
        {!loggedIn && <Redirect to="/" />}
            {loggedIn &&
        <AppContainer>
            <form className="mt-3" onSubmit={clearPhrases}>
                <h4>Your searches</h4>
                {array !==null && array.slice(-9).map(data => (
                    <li>{data.body}</li>
                ))}
                
                <button id="clear" type='submit'> clear history</button>
            </form>
            
            <form className="mt-3" onSubmit={doLogout}>
                
                <button id="logout" >logout</button> 
            </form>
           
            
        </AppContainer>
                }
                </>
    )
}
export default Profile
