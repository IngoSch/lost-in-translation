const TranslatedBox = ({ phrase, pathFinder }) => {//phrase and pathfinder are given as props
    
        const ImageList=phrase.replace(/ /g, "").split('') //Spaces replaced --> creates Array with letters inside users input
        ImageList.forEach((v,i)=> {ImageList[i] = pathFinder(v)})//parse the array and use the function to create the path of the letters image
        return (
        
            <form id="tr-output">
                <div>
                    {ImageList.map(data => (
                    <img src={data[0]} alt={data[1]}/>//index is needed pathFinder returns [path, item]
                    ))}
                </div>
            </form> 
    )
}

export default TranslatedBox;