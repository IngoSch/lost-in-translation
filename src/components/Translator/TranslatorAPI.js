
export const TranslatorAPI = {

    save(phraseList) {
        let format= phraseList.slice(-1)[0]
        return fetch('http://localhost:3001/phraselist', {
            method: 'POST',
            headers: {'Content-Type': 'application/json' },
            body: JSON.stringify(format),
        }).then(async (response) => {
            if (!response.ok) {
                const { error = 'An Error in the TranslatorAPI occured' } = await response.json()
                throw new Error(error.message)
            }
            return response.json()
            
            
        })
         
    },
    delete(){
        let len = localStorage.getItem('tr-ph').split('{').length        
        
        for (let i = 1; i <len; i++) {
                fetch(['http://localhost:3001/phraselist/'+i], {
                    method: 'DELETE',
                    headers: {'Content-Type':'application/json'},
                    body: null
                })      
            
                localStorage.removeItem('tr-ph')
               
            
        }      
           
    },

    logout(){
        let len = localStorage.getItem('tr-ph').split('{').length 
        console.log(len)       
        
        for (let i = 1; i <len; i++) {
                fetch(['http://localhost:3001/phraselist/'+i], {
                    method: 'DELETE',
                    headers: {'Content-Type':'application/json'},
                    body: null
                })
                fetch('http://localhost:3001/user/1', {
                    method: 'DELETE',
                    headers: {'Content-Type':'application/json'},
                    body: null
                }).then(localStorage.clear()
                )      
                console.log('got catched')
            
        }
        
              
        
        
    }
}