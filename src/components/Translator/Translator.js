import { useState } from "react"
import { useDispatch } from "react-redux"
import AppContainer from "../../hoc/AppContainer"
import { translatorDoAction } from "../../store/actions/translatorActions"
import TranslatedBox from "./TranslatedBox"
import { Link } from 'react-router-dom'


const phraselist = []
const arr = []
const Translator = () => {

    const dispatch = useDispatch()

    const [ phrase, setPhrase ] = useState({
        phrase: ""
    })
    const [ toTranslate, setToTranslate ] = useState("")

    const onInputChange = event => {
    
        setPhrase(
        {            
            ...phrase,
            [event.target.id]: event.target.value
        })
    }

    const getPhraseList = phrase =>{
        arr.push(phrase)
        for (let i in arr) {
            phraselist[i]={body: arr[i].phrase
            }
        }
        console.log(phraselist)
        return phraselist
    }

    const onFormSubmit = event => {
        setToTranslate(phrase.phrase)
        event.preventDefault()
        getPhraseList(phrase)
        console.log(phraselist)
        dispatch(translatorDoAction(phraselist))
    }

    const pathFinder = (item) => {
        let folderpath = "/assets/"
        let ending = ".png"
        const pathList = []
        let path = folderpath + item + ending
        pathList.push(path)
        return [pathList,item]
    }


    return (
    <AppContainer>
        <form id="tr-input" className="mt-3 mb-3" onSubmit={ onFormSubmit }>
            <input id="phrase"
            placeholder="Translate what!?"
            className="form-control"
            onChange= { onInputChange }></input>
            <button id="translate" type="submit" className="btn-lg">Translate</button>
        </form>
            <TranslatedBox phrase={toTranslate} pathFinder={ pathFinder }/>
            <Link to={'/profile'}>
                <button id="profile">Go to profilepage</button>
            </Link>
        
    </AppContainer>
    )
}

export default Translator