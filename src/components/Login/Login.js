import AppContainer from "../../hoc/AppContainer"
import { Redirect } from "react-router"
import { useState } from "react"
import { useDispatch, useSelector} from "react-redux"
import { loginAction } from "../../store/actions/loginActions"

const Login = () => {

    const dispatch = useDispatch()
    const { loggedIn } = useSelector(state => state.translatorReducer)

    const [ username, setUsername] = useState({
        username:'',
    })

    const onInputChange = event => {
        setUsername({
            ...username,
            [event.target.id]: event.target.value
        })
    }

    const onFormSubmit = event => {
        event.preventDefault()
        dispatch(loginAction(username))
    }

    return (
        
        <>
            {loggedIn && <Redirect to="/translator" />}
            {!loggedIn &&
        <AppContainer>
            <form className="mt-3" onSubmit={ onFormSubmit }>
            <div className="mb-3">
                <label htmlFor="username" className="form-label">Username</label>
                <input id="username" 
                type="text" 
                placeholder="Whats your name?" 
                className="form-control"
                onChange={ onInputChange }/>
            </div>

            <button type="submit" className="btn btn-primary btn-lg">Login</button>
            </form>
        </AppContainer>
        }
        </>

    )
}

export default Login