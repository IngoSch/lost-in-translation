import { Link } from 'react-router-dom';

const NotFound = () => {

    return (
        <main>
            <Link to="/">Take me home</Link>
        </main>
    )
}

export default NotFound