import {ACTION_LOGIN_ERROR, ACTION_LOGIN_SUCCESS } from "../actions/loginActions"

const initialState = {
    loginError: ''
}

export const loginReducer = ( state = initialState, action) => {
   
    switch(action.type) {
        case ACTION_LOGIN_ERROR:
            return {
                ...state,
                loginError: action.payload
            }
        case ACTION_LOGIN_SUCCESS:
            return {
                ...initialState
            }
        default:
            return {state}
    }
}