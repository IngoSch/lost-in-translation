import { combineReducers } from "redux";
import { loginReducer } from "./loginReducer";
import { translatorReducer } from "./translatorReducer"

const appReducer = combineReducers( {
    loginReducer,
    translatorReducer
})

export default appReducer