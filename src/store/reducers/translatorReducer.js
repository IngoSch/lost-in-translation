import { ACTION_TRANSLATOR_SET } from "../actions/translatorActions"

const initialState = {
    username: '',
    loggedIn: false
}

export const translatorReducer = ( state = initialState, action) => {
   
    switch(action.type) {
        case ACTION_TRANSLATOR_SET:
            return {
                ...action.payload,
                username: action.payload,
                loggedIn: true
            }
        default:
            return state
    }
}