export const ACTION_TRANSLATOR_SET = '[translator] SET'
export const ACTION_TRANSLATOR_INIT = '[translator] INIT'
export const ACTION_TRANSLATOR_DO = '[translator] DO'


export const translatorSetAction = username => ({
    type: ACTION_TRANSLATOR_SET,
    payload: username
})

export const translatorDoAction = phraselist => ({
    type: ACTION_TRANSLATOR_DO,
    payload: phraselist
})

export const translatorInitAction = () => ( {
    type: ACTION_TRANSLATOR_INIT,
})
