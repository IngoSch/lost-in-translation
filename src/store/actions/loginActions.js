export const ACTION_LOGIN_SUCCESS = '[login] SUCCESS';
export const ACTION_LOGIN_ERROR = '[login] ERROR'


export const loginAction = username => ({
    type: ACTION_LOGIN_SUCCESS,
    payload: username
})

export const loginErrorAction = error => ({
    type: ACTION_LOGIN_ERROR,
    payload: error
})