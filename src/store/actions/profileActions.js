export const ACTION_PROFILE_CLEAR = '[profile] CLEAR'
export const ACTION_PROFILE_LOGOUT = '[profile] LOGOUT'

export const profileClearAction = () => ({
    type: ACTION_PROFILE_CLEAR
})
export const profileLogoutAction = () => ({
    type: ACTION_PROFILE_LOGOUT
})