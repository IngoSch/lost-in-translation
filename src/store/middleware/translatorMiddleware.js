import { TranslatorAPI } from "../../components/Translator/TranslatorAPI"
import { ACTION_TRANSLATOR_DO, ACTION_TRANSLATOR_INIT, ACTION_TRANSLATOR_SET, translatorSetAction } from "../actions/translatorActions"


export const translatorMiddleware = ({ dispatch }) => next => action => { //Those functions have the action as payload, that´s why the payload is given aswell.

    next(action)//has to be in the beginning, because of possibility to use another function!?

    if (action.type === ACTION_TRANSLATOR_INIT) {//checks for the type of the payload to cast new functions specified to the type.
        const storedSession = localStorage.getItem('tr-s')

        if(!storedSession) {
            return
        }
        const session = JSON.parse(storedSession)
        dispatch(translatorSetAction(session))
    }

    if (action.type === ACTION_TRANSLATOR_SET) {
        localStorage.setItem('tr-s', JSON.stringify(action.payload))
    }

    if (action.type === ACTION_TRANSLATOR_DO) {
        localStorage.setItem('tr-ph', JSON.stringify(action.payload))
        TranslatorAPI.save(action.payload)
    }   

    
}