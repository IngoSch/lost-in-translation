
import { applyMiddleware } from "redux";
import { loginMiddleware } from "./loginMiddleware";
import { profileMiddleware } from "./profileMiddleware";
import { translatorMiddleware } from "./translatorMiddleware";

export default applyMiddleware(
    loginMiddleware,
    translatorMiddleware,
    profileMiddleware
)