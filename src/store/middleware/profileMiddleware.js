import { TranslatorAPI } from "../../components/Translator/TranslatorAPI"
import { ACTION_PROFILE_CLEAR, ACTION_PROFILE_LOGOUT } from "../actions/profileActions"

export const profileMiddleware = ({ dispatch }) => next => action => {
    next(action) //put it in the beginning of the script

    if (action.type === ACTION_PROFILE_CLEAR) {
        TranslatorAPI.delete()
    }

    if (action.type === ACTION_PROFILE_LOGOUT) {
        TranslatorAPI.logout()
    }
    /*--Insert the actions to push here--*/
}