
import { LoginAPI } from "../../components/Login/LoginAPI"
import { ACTION_LOGIN_SUCCESS} from "../actions/loginActions"
import { translatorSetAction } from "../actions/translatorActions"

export const loginMiddleware = ({ dispatch }) => next => action => {
    next(action) //put it in the beginning of the script

    if (action.type === ACTION_LOGIN_SUCCESS) {
        LoginAPI.login(action.payload)
        console.log("Login successful")
        dispatch(translatorSetAction(action.payload))
    }
    /*--Insert the actions to push here--*/
}