import { createStore } from "redux";
import middleware from "./middleware";
import appReducer from "./reducers/index";
import { composeWithDevTools } from "redux-devtools-extension"

export default createStore(
    appReducer,
    composeWithDevTools(middleware)
)